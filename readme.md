Here you can find all material used for experiments and the link for the paper: http://www.sciencedirect.com/science/article/pii/S2212683X17300063
 					
You are free for use as the base for new researchers or improve it, since reference this work.

FILES:

ICPSR_09329: real data used in experiments.
You can find the rouge data and a worksheet with processed data. In that worksheet you can see how the variables was counted for each input/output of the model. 
Data.xlsx ds1, ds2, ds3, ... are the sheets with rouge data.
Each column represent one variable, the used variables are identified by its name relative to description in: 09329-Codebook.pdf
trauma, trigger, social support, ... are the compilation of variables used. Each line is a register (different real case/person).

You can see how variables were counted by observing the formulas inside cells. Each variable is composed by 3 columns. Example: CUTOFF | weight | var value
where:
CUTOFF is the variable and its rouge value
Weight is the importance of that variable in the sum of values for the input of the model, for this case: SOCIAL SUPPORT
var value is the final value of this variable for the input of the model.
at right of each sheet you find one or more of this tags: FI-N	FI-P	SI-N	SI-P, they means: FI-N: 
FIRST INTERVIEW-NEGAVIVE IMPACT, 
FI-N: FIRST INTERVIEW-POSITIVE IMPACT,
SI-N: SECOND INTERVIEW-NEGAVIVE IMPACT,
SI-N: SECOND INTERVIEW-POSITIVE IMPACT
This columns aggregate the sum of values of variables and are the inputs of the model.

We just used the second interview value in our experiments 
We only used complete registers without missing data, that is found in: data_resume.xlsx
Other files in this folder correspond to original database. See link-to-database 
matlab_v160202: last matlab version.

	All files named t_* is perform a different test. Other files are auxiliary files used by the tests.
    	data.m have the data imported from excel file.

