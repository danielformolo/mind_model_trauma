Bibliography
Helping Crime Victims:  Levels of Trauma and Effectiveness of Services in
Arizona, 1983-1984

This bibliography includes published and unpublished works that are related 
to ICPSR study 9329. It includes works that are based on primary
or secondary analysis of the data, or which describe or critique those
data or the collection methodology. This list represents all items known
to ICPSR as of 2016-01-07.

If you publish a work or know of other works that are based on these data,
please send the complete citation and name of the ICPSR study used to:
bibliography@icpsr.umich.edu.


1. Cook, Royer F.; Smith, Barbara E.; Harrell, Adele V., "Helping Crime
Victims: Levels of Trauma and Effectiveness of Services, Executive
Summary." NCJ 100868, Washington, DC: United States Department of Justice,
National Institute of Justice, Jan 1986. 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100868
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100868NCJRS.pdf

2. Harrell, Adele V.; Smith, Barbara E.; Cook, Royer F., "The Social
Psychological Effects of Victimization, Final Report." NCJ 100867,
Washington, DC: United States Department of Justice, National Institute of
Justice, 1986. 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100867
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100867NCJRS.pdf
Victims of rape, domestic assault, other assaults, burglary, and robbery
were interviewed 1 month after victimization and again 4 to 6 months later
about the social, psychological, financial, and physical effects of their
victimization. Five scales of psychological distress measured fear,
anxiety, stress, dismay, and social adjustment. Information was also
obtained on prior life stress and victimization. Within a month of the
crime, victims showed high distress levels for all measures. Distress was
highest among victims of more severe crimes; women manifested more distress
than men. Four to 6 months later, symptoms of distress other than fear had
abated considerably. Distress was more pronounced among victims with higher
stress levels more pronounced among victims with higher stress levels in
the year prior to victimization. There was only slight evidence that crisis
intervention relieved psychological distress. Implications are drawn for
practitioners and researchers. Appendixes contain tabular data and study
scales. 58 references.

3. Smith, Barbara E.; Cook, Royer F.; Harrell, Adele V., "Evaluation of
Victim Services, Final Report." NCJ 100869, Washington, DC: United States
Department of Justice, National Institute of Justice, 1985. 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100869
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100869NCJRS.pdf
A quasi-experimental design compared victims who received crisis
intervention services (109), delayed services (114), and no services (100)
through interviews 1 month after the crime and 4 to 6 months later. The
psychological, social, financial, and physical impact of victimization was
measured. The impact of victim services on police and prosecutors was
assessed through surveys and group interviews. Process data were collected
through interviews and observations of program staff. Both crisis
intervention and delayed services assisted victims in various ways, but
there was only slight evidence that these services reduced the victims'
emotional trauma. Both police and prosecutors generally valued the victim
services for the performance of their duties, but neither used the services
to capacity. Findings suggest that jurisdictions without victim services
should consider establishing them and that existing programs address the
underuse of such services. Appendixes detail volunteer training and duties,
observations of the crisis unit, victim demographics, study methodology,
and police and prosecutor survey results. Tabular data and 42 references.

4. Wirtz, P.; Harrell, A., "Assaultive vs. Nonassaultive Victimization: A
Profile Analysis." Journal of Interpersonal Violence. 1987, 2, (3), 264 -
277. DOI: 10.1177/088626087002003003 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100869
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100869NCJRS.pdf
A quasi-experimental design compared victims who received crisis
intervention services (109), delayed services (114), and no services (100)
through interviews 1 month after the crime and 4 to 6 months later. The
psychological, social, financial, and physical impact of victimization was
measured. The impact of victim services on police and prosecutors was
assessed through surveys and group interviews. Process data were collected
through interviews and observations of program staff. Both crisis
intervention and delayed services assisted victims in various ways, but
there was only slight evidence that these services reduced the victims'
emotional trauma. Both police and prosecutors generally valued the victim
services for the performance of their duties, but neither used the services
to capacity. Findings suggest that jurisdictions without victim services
should consider establishing them and that existing programs address the
underuse of such services. Appendixes detail volunteer training and duties,
observations of the crisis unit, victim demographics, study methodology,
and police and prosecutor survey results. Tabular data and 42 references.

5. Wirtz, P.; Harrell, A., "The Effects of Threatening vs. Nonthreatening
Previous Life Events on Fear Levels in Rape Victims." Violence and Victims.
1987, 2, (2), 89 - 97. 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100869
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100869NCJRS.pdf
A quasi-experimental design compared victims who received crisis
intervention services (109), delayed services (114), and no services (100)
through interviews 1 month after the crime and 4 to 6 months later. The
psychological, social, financial, and physical impact of victimization was
measured. The impact of victim services on police and prosecutors was
assessed through surveys and group interviews. Process data were collected
through interviews and observations of program staff. Both crisis
intervention and delayed services assisted victims in various ways, but
there was only slight evidence that these services reduced the victims'
emotional trauma. Both police and prosecutors generally valued the victim
services for the performance of their duties, but neither used the services
to capacity. Findings suggest that jurisdictions without victim services
should consider establishing them and that existing programs address the
underuse of such services. Appendixes detail volunteer training and duties,
observations of the crisis unit, victim demographics, study methodology,
and police and prosecutor survey results. Tabular data and 42 references.

6. Writz, P.; Harrell, A., "Effects of Exposure to Attack-Similar Stimuli
on Long-Term Recovery of Victims." Journal of Consulting and Clinical
Psychology. 1987, 55, (1), 10 - 16. 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100869
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100869NCJRS.pdf
A quasi-experimental design compared victims who received crisis
intervention services (109), delayed services (114), and no services (100)
through interviews 1 month after the crime and 4 to 6 months later. The
psychological, social, financial, and physical impact of victimization was
measured. The impact of victim services on police and prosecutors was
assessed through surveys and group interviews. Process data were collected
through interviews and observations of program staff. Both crisis
intervention and delayed services assisted victims in various ways, but
there was only slight evidence that these services reduced the victims'
emotional trauma. Both police and prosecutors generally valued the victim
services for the performance of their duties, but neither used the services
to capacity. Findings suggest that jurisdictions without victim services
should consider establishing them and that existing programs address the
underuse of such services. Appendixes detail volunteer training and duties,
observations of the crisis unit, victim demographics, study methodology,
and police and prosecutor survey results. Tabular data and 42 references.

7. Writz, P.; Harrell, A., "Police and Victims of Physical Assault."
Criminal Justice and Behavior. 1987, 14, (1), 81 - 92. 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100869
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100869NCJRS.pdf
A quasi-experimental design compared victims who received crisis
intervention services (109), delayed services (114), and no services (100)
through interviews 1 month after the crime and 4 to 6 months later. The
psychological, social, financial, and physical impact of victimization was
measured. The impact of victim services on police and prosecutors was
assessed through surveys and group interviews. Process data were collected
through interviews and observations of program staff. Both crisis
intervention and delayed services assisted victims in various ways, but
there was only slight evidence that these services reduced the victims'
emotional trauma. Both police and prosecutors generally valued the victim
services for the performance of their duties, but neither used the services
to capacity. Findings suggest that jurisdictions without victim services
should consider establishing them and that existing programs address the
underuse of such services. Appendixes detail volunteer training and duties,
observations of the crisis unit, victim demographics, study methodology,
and police and prosecutor survey results. Tabular data and 42 references.

8. Writz, P.; Harrell, A., "Victim and Crime Characteristics, Coping
Response, and Short and Long-Term Recovery From Victimization." Journal of
Consulting and Clinical Psychology. 1987, 55, 866 - 871. 82-IJ-CX-KO36
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100869
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100869NCJRS.pdf
A quasi-experimental design compared victims who received crisis
intervention services (109), delayed services (114), and no services (100)
through interviews 1 month after the crime and 4 to 6 months later. The
psychological, social, financial, and physical impact of victimization was
measured. The impact of victim services on police and prosecutors was
assessed through surveys and group interviews. Process data were collected
through interviews and observations of program staff. Both crisis
intervention and delayed services assisted victims in various ways, but
there was only slight evidence that these services reduced the victims'
emotional trauma. Both police and prosecutors generally valued the victim
services for the performance of their duties, but neither used the services
to capacity. Findings suggest that jurisdictions without victim services
should consider establishing them and that existing programs address the
underuse of such services. Appendixes detail volunteer training and duties,
observations of the crisis unit, victim demographics, study methodology,
and police and prosecutor survey results. Tabular data and 42 references.

9. Writz, Philip W.; Harrell, Adele V., "Victim and crime characteristics,
coping responses, and short- and long-term recovery from victimization."
Journal of Consulting and Clinical Psychology. Dec 1987, 55, (6), 866 -
871.
Abstract: https://www.ncjrs.gov/App/Publications/abstract.aspx?ID=100869
Full text PDF: https://www.ncjrs.gov/pdffiles1/Digitization/100869NCJRS.pdf
A quasi-experimental design compared victims who received crisis
intervention services (109), delayed services (114), and no services (100)
through interviews 1 month after the crime and 4 to 6 months later. The
psychological, social, financial, and physical impact of victimization was
measured. The impact of victim services on police and prosecutors was
assessed through surveys and group interviews. Process data were collected
through interviews and observations of program staff. Both crisis
intervention and delayed services assisted victims in various ways, but
there was only slight evidence that these services reduced the victims'
emotional trauma. Both police and prosecutors generally valued the victim
services for the performance of their duties, but neither used the services
to capacity. Findings suggest that jurisdictions without victim services
should consider establishing them and that existing programs address the
underuse of such services. Appendixes detail volunteer training and duties,
observations of the crisis unit, victim demographics, study methodology,
and police and prosecutor survey results. Tabular data and 42 references.
