%----------------------------------------------
clear
for experiment=1:3
   
% INITIAL SETUP
close all
clc
empiricaldata = importfile('samples.mat');

if experiment==3
    number_of_agents = 4; % TUNE: number of agents
else
    number_of_agents = 1; % TUNE: number of agents
end


delta_t = 0.2;
speed_factor = 0.02;
steps = 20000;        % TUNE: rounds, global end time
number_states = 21;

start_trauma        = 500;
end_trauma          = 1500;
trigger_on_period   = 1000;
trigger_off_period  = 10000;

%
% TUNE: DISSOCIATION DIRECT
%
steepness = 4.00;
threshold = 0.350;

learn_rate_supress1     = 0.020;
extintion_rate_supress1 = 0.000;
learn_rate_trauma1      = 0.900;    %w18 and w28
learn_rate_trauma2      = 0.001;    %w17 and w27
extintion_rate_trauma1  = 0.000;
%w20=0.5, w16=0.9, w26=0.9.
%w6=0.9

relations1 = [0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0.5, 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,-0.5,    0.5,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0.5, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 4
              0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.9, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 8
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.505,   0  , 0  , 0  , 0.5,   0.10,  0  ,0.505, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.9, 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.505,0  , 0  , 0  ,   0  , 0  , 0  , 0.1,    0.5,  0  ,0.505, 0  ,   0  ; %12
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.5, 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.5,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.5,     0  ,  0  , 0  , 0.5,   0  ; %16
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  , -0.9 , 0  , 0  ,-0.9,   0  , 0  , 0  ,-0.5,    0  , 0.505,0.505, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.505, 0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,  0.505, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.505, 0.5, 0  , 0  ,   0.99;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; %20
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;];
          
mrelations = zeros( number_of_agents, number_states, number_states);
for k=1:number_of_agents
    for i=1:number_states
        for j=1:number_states
            mrelations(k,i,j) = relations1(i,j);
        end
    end
end

vet_error = zeros(257, 1);
for sample=196:196
    [states, relations_new, vet_weight] = simulation( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress1, extintion_rate_supress1, learn_rate_trauma1, learn_rate_trauma2, extintion_rate_trauma1,  steepness, threshold, empiricaldata, sample, start_trauma, end_trauma, trigger_on_period, trigger_off_period, experiment);
    time = 1000+empiricaldata(sample,10)*10;
    vet_error(sample-1) = abs(states(k,21,time) - empiricaldata(sample,9));
end

for k=1:number_of_agents  % define which agents plot. ex.: k=2:3 print behavior of agents 2 and 3.
    if experiment==1
        mat1 = zeros(number_states, steps);
        for i=1:number_states
            for j=1:steps
                mat1(i,j) = states(k,i,j);
            end
        end
    elseif experiment==2
        mat2 = zeros(number_states, steps);
        for i=1:number_states
            for j=1:steps
                mat2(i,j) = states(k,i,j);
            end
        end
    elseif experiment==3 && k==1
        mat3 = zeros(number_states, steps);
        for i=1:number_states
            for j=1:steps
                mat3(i,j) = states(k,i,j);
            end
        end
    end        
end
end


hold all;
xlabel('time') 
ylabel('states value')
set(gca,'fontsize',24)

x = 1:steps;
y = mat1(21,:); plot(x(1:200:steps), y(1:200:steps), '-rO', 'MarkerSize',8); % Emotional Response.
y = mat2(21,:); plot(x(1:200:steps), y(1:200:steps), '-kx', 'MarkerSize',8); % Emotional Response.
y = mat3(21,:); plot(x(1:200:steps), y(1:200:steps), '-b', 'MarkerSize',8); % Emotional Response.
%set( graph_legend, 'FontSize', 22);    
%graph_legend = legend(                                          'No Support', 'Support', 'Group Support');
legend(                                          'No Support', 'Support', 'Group Support');    
hold off;
