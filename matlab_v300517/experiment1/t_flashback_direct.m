%----------------------------------------------
% INITIAL SETUP
clear
close all
clc
empiricaldata = importfile('samples.mat');

delta_t = 0.2;
speed_factor = 0.02;
number_of_agents = 1; % TUNE: number of agents
steps = 20000;         % TUNE: rounds, global end time
number_states = 21;

start_trauma        = 500;
end_trauma          = 1500;
trigger_on_period   = 1000;
trigger_off_period  = 10000;

%
% TUNE: FLASHBACK DIRECT
%
steepness = 4.00;
threshold = 0.350;

learn_rate_supress1     = 0.001;
extintion_rate_supress1 = 0.000;
learn_rate_trauma1      = 0.900;   %w18 and w28
learn_rate_trauma2      = 0.001;   %w17 and w27
extintion_rate_trauma1  = 0.000;
%w20=0.1, w16=0.5, w26=0.5
%w6=0.9

relations1 = [0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0.5, 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,-0.5,    0.5,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0.5, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 4
              0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.9, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 8
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.505,   0  , 0  , 0  , 0.5,   0.10,  0  ,0.505, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.9, 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.505,0  , 0  , 0  ,   0  , 0  , 0  , 0.1,    0.1,  0  ,0.505, 0  ,   0  ; %12
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.5, 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.5,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.5,     0  ,  0  , 0  , 0.5,   0  ; %16
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  , -0.5 , 0  , 0  ,-0.5   0  , 0  , 0  ,-0.1,    0  , 0.505,0.505, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.505, 0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,  0.505, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.505, 0.5, 0  , 0  ,   0.99;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; %20
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;];
          
mrelations = zeros( number_of_agents, number_states, number_states);
for k=1:number_of_agents
    for i=1:number_states
        for j=1:number_states
            mrelations(k,i,j) = relations1(i,j);
        end
    end
end

vet_error = zeros(257, 1);
for sample=196:196
    [states, relations_new, vet_weight] = simulation( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress1, extintion_rate_supress1, learn_rate_trauma1, learn_rate_trauma2, extintion_rate_trauma1,  steepness, threshold, empiricaldata, sample, start_trauma, end_trauma, trigger_on_period, trigger_off_period);
    time = 1000+empiricaldata(sample,10)*10;
    vet_error(sample-1) = abs(states(k,21,time) - empiricaldata(sample,9));
end

hold all;
xlabel('time') 
ylabel('states value')
set(gca,'fontsize',24)
for k=1:number_of_agents  % define which agents plot. ex.: k=2:3 print behavior of agents 2 and 3.
    mat = zeros(number_states, steps);
    for i=1:number_states
        for j=1:steps
            mat(i,j) = states(k,i,j);
        end
    end

    x = 1:steps;    
% %     y = mat(1,:); plot(x(1:200:steps), y(1:200:steps), '-m');   % TUNE: plot Positive Social Event.
% %     y = mat(4,:); plot(x(1:200:steps), y(1:200:steps), '--m');  % TUNE: plot Negative Contagion.
% %     y = mat(7,:); plot(x(1:200:steps), y(1:200:steps), ':m');   % TUNE: plot Trauma Event.
% %     y = mat(10,:); plot(x(1:200:steps), y(1:200:steps),'-.m');  % TUNE: plot Trigger Event.
% %     y = mat(13,:); plot(x(1:200:steps), y(1:200:steps), '-m+'); % TUNE: plot Environment Event.
% %     y = mat(20,:); plot(x(1:200:steps), y(1:200:steps), '-bo'); % TUNE: plot Trauma Display.
% %     y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-r>'); % TUNE: plot Emotional Response.
% %     y = vet_weight; plot(x(1:200:steps), y(1:200:steps), '-rx'); % TUNE: plot one weight.
%     y = mat(1,:); plot(x(1:200:steps), y(1:200:steps), '-k');   % TUNE: plot Positive Social Event.
%     y = mat(4,:); plot(x(1:200:steps), y(1:200:steps), '--k');  % TUNE: plot Negative Contagion.
%     y = mat(7,:); plot(x(1:200:steps), y(1:200:steps), ':k');   % TUNE: plot Trauma Event.
%     y = mat(10,:); plot(x(1:200:steps), y(1:200:steps),'-.k');  % TUNE: plot Trigger Event.
%     y = mat(13,:); plot(x(1:200:steps), y(1:200:steps), '-k+'); % TUNE: plot Environment Event.
%     y = mat(20,:); plot(x(1:200:steps), y(1:200:steps), '-ko'); % TUNE: plot Trauma Display.
%     y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-k>'); % TUNE: plot Emotional Response.
%     y = vet_weight; plot(x(1:200:steps), y(1:200:steps), '-kx'); % TUNE: plot one weight.

%      x = 1:steps;
%      for m=5:9
%          if m~=3 && m~=1
%              if m==8
%                 y = vet_weight(m,1:steps); plot(x(1:200:steps), y(1:200:steps), '-x');   % TUNE: plot Positive Social Event.
%              else
%                  y = vet_weight(m,1:steps); plot(x(1:200:steps), y(1:200:steps), '-');   % TUNE: plot Positive Social Event.
%              end
%          end
%      end
%     y = mat(07,:); plot(x(1:200:steps), y(1:200:steps), '-.');   % TUNE: plot Trauma Event.
%     plot(mat(10,:),':');  % TUNE: plot Trigger Event.
%     y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '->');   % TUNE: plot Emotional Response.
%     y = mat(09,:); plot(x(1:200:steps), y(1:200:steps), '-o');   % TUNE: plot SRte.
    
end
     y = vet_weight(5,1:steps); plot(x(1:200:steps), y(1:200:steps), '-k', 'MarkerSize',8, 'LineWidth', 1);
     y = vet_weight(6,1:steps); plot(x(1:200:steps), y(1:200:steps), '--k', 'MarkerSize',8, 'LineWidth', 1.5);
     y = vet_weight(7,1:steps); plot(x(1:350:steps), y(1:350:steps), '-kx', 'MarkerSize',8);
     y = vet_weight(8,1:steps); plot(x(1:350:steps), y(1:350:steps), '-ko', 'MarkerSize',10);
     y = vet_weight(9,1:steps); plot(x(1:350:steps), y(1:350:steps), '-k+', 'MarkerSize',8);
     y = mat(07,:); plot(x(1:200:steps), y(1:200:steps), ':k', 'MarkerSize',8, 'LineWidth', 1.5); % Trauma
     y = mat(10,:); plot(x(1:200:steps), y(1:200:steps), '-.k', 'MarkerSize',8, 'LineWidth', 1.5); % Trigger
     y = mat(09,:); plot(x(1:350:steps), y(1:350:steps), '--ko', 'MarkerSize',10);
     y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-k>', 'MarkerSize',8); % Emotional Response.

graph_legend = legend(                                          '\omega28', '\omega17', '\omega27', '\omega25', '\omega18', 'WS_{te}', 'WS_{tr}',  'srs_{te}, es_b', 'es_b');
set( graph_legend, 'FontSize', 22);

%legend('Real Data', 'w33', 'w23', 'w30', 'w24', 'w28', 'w17', 'w27', 'w25', 'w18', 'Input Trauma', 'Input Trigger', 'Emotional Response');
%legend('Input Social Support', 'Input Negative Contagion', 'Input Trauma', 'Input Trigger', 'Input Environment', 'Trauma Display', 'Emotional Response', 'Real Data', 'Weight 27');
hold off;
