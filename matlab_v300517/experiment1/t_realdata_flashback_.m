%----------------------------------------------
% INITIAL SETUP
clear
close all
clc
empiricaldata = importfile('samples.mat');

delta_t = 0.2;
speed_factor = 0.02;
number_of_agents = 1; % TUNE: number of agents
steps = 7000;         % TUNE: rounds, global end time
number_states = 21;
steepness = 3.15;
threshold = 0.02;
%
% TUNE: FLASHBACK AGENT
%
 learn_rate_supress1 = 0.009;      % hebbian connection
 extintion_rate_supress1 = 0.00; % hebbian connection
 learn_rate_trauma1 = 0.35;       % hebbian connection
 extintion_rate_trauma1 = 0.00;  % hebbian connection
%
% FOR SAMPLE 196, register #625, relation 12->19 started very low: 0.01 and finished high:
% 0.8085 characterizing a flashback personality with strong connection
% between preparation state and trigger events.
%samples: G1
%
relations1 = [0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0.1, 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,-0.5,    0.1,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0.5, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 4
              0  , 0  , 0  , 0  ,   0  , 0.9, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.9, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 8
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.00,   0  , 0  , 0  , 0.5,   0.01,  0  ,0.00, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.5, 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.00,0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.5,  0  ,0.00, 0  ,   0 ; %12
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.2, 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.5,  0  ,-0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.1,     0  ,  0  , 0  , 0.5,   0  ; %16
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  , -0.001,0  , 0  ,-0.5,   0  , 0  , 0  ,-0.5,    0  , 0.00,0.00, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.00, 0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,  0.00, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.00, 0.99, 0  , 0  ,  0.9;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; %20
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;];

mrelations = zeros( number_of_agents, number_states, number_states);
for k=1:number_of_agents
    for i=1:number_states
        for j=1:number_states
            mrelations(k,i,j) = relations1(i,j);
        end
    end
end

vet_error = zeros(257, 1);
for sample=196:196
    [states, relations_new, vet_weight] = simulation( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress1, extintion_rate_supress1, learn_rate_trauma1, extintion_rate_trauma1,  steepness, threshold, empiricaldata, sample);
    time = 1000+empiricaldata(sample,10)*10;
    vet_error(sample-1) = abs(states(k,21,time) - empiricaldata(sample,9));
end

hold all;
xlabel('time') 
ylabel('states value')
for k=1:number_of_agents  % define which agents plot. ex.: k=2:3 print behavior of agents 2 and 3.
    mat = zeros(number_states, steps);
    for i=1:number_states
        for j=1:steps
            mat(i,j) = states(k,i,j);
        end
    end

    x = 1:steps;    
    plot(1000+empiricaldata(sample,10)*10,empiricaldata(sample,9),'k*', 'MarkerSize',15);    
% %     y = mat(1,:); plot(x(1:200:steps), y(1:200:steps), '-m');   % TUNE: plot Positive Social Event.
% %     y = mat(4,:); plot(x(1:200:steps), y(1:200:steps), '--m');  % TUNE: plot Negative Contagion.
% %     y = mat(7,:); plot(x(1:200:steps), y(1:200:steps), ':m');   % TUNE: plot Trauma Event.
% %     y = mat(10,:); plot(x(1:200:steps), y(1:200:steps),'-.m');  % TUNE: plot Trigger Event.
% %     y = mat(13,:); plot(x(1:200:steps), y(1:200:steps), '-m+'); % TUNE: plot Environment Event.
% %     y = mat(20,:); plot(x(1:200:steps), y(1:200:steps), '-bo'); % TUNE: plot Trauma Display.
% %     y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-r>'); % TUNE: plot Emotional Response.
% %     y = vet_weight; plot(x(1:200:steps), y(1:200:steps), '-rx'); % TUNE: plot one weight.
%     y = mat(1,:); plot(x(1:200:steps), y(1:200:steps), '-k');   % TUNE: plot Positive Social Event.
%     y = mat(4,:); plot(x(1:200:steps), y(1:200:steps), '--k');  % TUNE: plot Negative Contagion.
%     y = mat(7,:); plot(x(1:200:steps), y(1:200:steps), ':k');   % TUNE: plot Trauma Event.
%     y = mat(10,:); plot(x(1:200:steps), y(1:200:steps),'-.k');  % TUNE: plot Trigger Event.
%     y = mat(13,:); plot(x(1:200:steps), y(1:200:steps), '-k+'); % TUNE: plot Environment Event.
%     y = mat(20,:); plot(x(1:200:steps), y(1:200:steps), '-ko'); % TUNE: plot Trauma Display.
%     y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-k>'); % TUNE: plot Emotional Response.
%     y = vet_weight; plot(x(1:200:steps), y(1:200:steps), '-kx'); % TUNE: plot one weight.

    x = 1:steps;
     for m=5:9
         if m~=3 && m~=1
             if m==8
                y = vet_weight(m,1:steps); plot(x(1:200:steps), y(1:200:steps), '-x');   % TUNE: plot Positive Social Event.
             else
                 y = vet_weight(m,1:steps); plot(x(1:200:steps), y(1:200:steps), '-');   % TUNE: plot Positive Social Event.
             end
         end
     end
    y = mat(07,:); plot(x(1:200:steps), y(1:200:steps), '-.');   % TUNE: plot Trauma Event.
    plot(mat(10,:),':');  % TUNE: plot Trigger Event.
    y = mat(20,:); plot(x(1:200:steps), y(1:200:steps), '->');  % TUNE: plot Emotional Response.
    y = mat(09,:); plot(x(1:200:steps), y(1:200:steps), '-o');  % TUNE: plot Emotional Response.


end

legend('Real Data',                              'w28', 'w17', 'w27', 'w25', 'w18', 'Input Trauma', 'Input Trigger', 'Emotional Response', 'SRte');
%legend('Real Data', 'w33', 'w23', 'w30', 'w24', 'w28', 'w17', 'w27', 'w25', 'w18', 'Input Trauma', 'Input Trigger', 'Emotional Response');
%legend('Input Social Support', 'Input Negative Contagion', 'Input Trauma', 'Input Trigger', 'Input Environment', 'Trauma Display', 'Emotional Response', 'Real Data', 'Weight 27');
hold off;
