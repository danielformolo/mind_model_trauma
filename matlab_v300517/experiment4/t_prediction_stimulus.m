%----------------------------------------------
% INITIAL SETUP
clear
close all
clc
empiricaldata = importfile('samples.mat');

delta_t = 0.08;
speed_factor = 0.04;
number_of_agents = 1; % TUNE: number of agents
steps = 5000;         % TUNE: rounds, global end time
number_states = 21;

% % TUNE: DISSOSSIATIVE AGENT
  learn_rate_supress3 = 0.6;         % hebbian connection
  extintion_rate_supress3 = 0.05;    % hebbian connection
  learn_rate_trauma3 = 0.6;       % hebbian connection
  extintion_rate_trauma3 = 0.6;   % hebbian connection

relations3 =  [0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0.5, 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,-0.5,    0.5,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0.5, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 4
               0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0.5, 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.5, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 8
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.01,   0  , 0  , 0  , 0.5,   0.01,  0  ,0.01, 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.5, 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.01,0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.1,  0  ,0.01, 0  ,   0 ; %12
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.5, 0  ,    0  ,  0  , 0  , 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.1,  0  ,-0.5, 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.1,     0  ,  0  , 0  , 0.5,   0  ; %16
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  , -0.001,0  , 0  ,-0.9,   0  , 0  , 0  ,-0.5,    0  ,-0.01,-0.01, 0  ,  0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.01, 0  , 0.5, 0  ,   0  ;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.4, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.01, 0.9, 0  , 0  ,   0.9;
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; %20
               0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;];
         
mrelations = zeros( number_of_agents, number_states, number_states);
for k=1:number_of_agents
    for i=1:number_states
        for j=1:number_states
            if (mod(k,2)==1)
                mrelations(k,i,j) = relations3(i,j);
            else
                mrelations(k,i,j) = relations3(i,j);
            end
        end
    end
end

vet_error = zeros(257, 1);
for sample=2:2
    [states, relations_new]= prediction_stimulus_simulation( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress3, extintion_rate_supress3, learn_rate_trauma3, extintion_rate_trauma3, empiricaldata, sample, 0);
    [states2, relations_new2]= prediction_stimulus_simulation( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress3, extintion_rate_supress3, learn_rate_trauma3, extintion_rate_trauma3, empiricaldata, sample, 0.5);
    time = 1000+empiricaldata(sample,10)*10;
    vet_error(sample-1) = abs(states(k,21,time) - empiricaldata(sample,9));
end

hold all;
xlabel('time') 
ylabel('states value')
% title('Control State: output weights=0.1, input weights=0.1');
for k=1:number_of_agents  % define which agents plot. ex.: k=2:3 print behavior of agents 2 and 3.
    mat  = zeros(number_states, steps);
    mat2 = zeros(number_states, steps);
    for i=1:number_states
        for j=1:steps
            mat(i,j) = states(k,i,j);
            mat2(i,j) = states2(k,i,j);
        end
    end
    
    x = 1:steps;
    y = mat(7,:); plot(x(1:steps), y(1:steps), 'k');   % TUNE: plot Trauma Event.
    y = mat(20,:); plot(x(1:200:steps), y(1:200:steps), '-ko'); % TUNE: plot Trauma Display.
    y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-k>'); % TUNE: plot Emotional Response.
    y = mat2(20,:); plot(x(1:200:steps), y(1:200:steps), ':ko'); % TUNE: plot Trauma Display.
    y = mat2(21,:); plot(x(1:200:steps), y(1:200:steps), ':k>'); % TUNE: plot Emotional Response.
    plot(1000+empiricaldata(sample,10)*10,empiricaldata(sample,9),'k*','MarkerSize',15);

end
legend('Input Trauma', 'Trauma Display - original social support', 'Emotional Response - original social support', 'Trauma Display - plus 20% social support','Emotional Response - plus 20% social support', 'Real Data');

hold off;
