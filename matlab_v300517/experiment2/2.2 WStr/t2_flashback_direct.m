%----------------------------------------------
% INITIAL SETUP
clear
close all
clc
empiricaldata = importfile('samples.mat');

delta_t = 0.2;
speed_factor = 0.02;
number_of_agents = 1; % TUNE: number of agents
steps = 50000;         % TUNE: rounds, global end time
number_states = 21;

start_trauma        = 500;
end_trauma          = 1500;
trigger_on_period   = 500;
trigger_off_period  = 2000;

%
% TUNE: FLASHBACK DIRECT
%
steepness = 4.00;
threshold = 0.350;

learn_rate_supress1     = 0.001;
extintion_rate_supress1 = 0.000;
learn_rate_trauma1      = 0.900;   %w18 and w28
learn_rate_trauma2      = 0.001;   %w17 and w27
extintion_rate_trauma1  = 0.000;
%w20=0.1, w16=0.5, w26=0.5
%w6=0.9

relations1 = [0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0.5, 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,-0.5,    0.5,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0.5, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 4
              0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.9, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 8
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.505,   0  , 0  , 0  , 0.5,   0.10,  0  ,0.505, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.9, 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.505,0  , 0  , 0  ,   0  , 0  , 0  , 0.1,    0.1,  0  ,0.505, 0  ,   0  ; %12
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.5, 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.5,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.5,     0  ,  0  , 0  , 0.5,   0  ; %16
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  , -0.5 , 0  , 0  ,-0.5   0  , 0  , 0  ,-0.1,    0  , 0.505,0.505, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.505, 0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,  0.505, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.505, 0.5, 0  , 0  ,   0.99;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; %20
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;];
          
mrelations = zeros( number_of_agents, number_states, number_states);
for k=1:number_of_agents
    for i=1:number_states
        for j=1:number_states
            mrelations(k,i,j) = relations1(i,j);
        end
    end
end

hold all;
xlabel('time') 
ylabel('es_b')
set(gca,'fontsize',24)

% MULTIPLE TRIGGERS: 
% 4  no trigger
% 5  several triggers
% 6  constant trigger
vet_error = zeros(257, 1);
for sample=196:196
    [states, relations_new, vet_weight] = simulation_flashback_direct( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress1, extintion_rate_supress1, learn_rate_trauma1, learn_rate_trauma2, extintion_rate_trauma1,  steepness, threshold, empiricaldata, sample, start_trauma, end_trauma, trigger_on_period, trigger_off_period, 4);
    time = 1000+empiricaldata(sample,10)*10;
    vet_error(sample-1) = abs(states(k,21,time) - empiricaldata(sample,9));
end
for k=1:number_of_agents  % define which agents plot. ex.: k=2:3 print behavior of agents 2 and 3.
    mat = zeros(number_states, steps);
    for i=1:number_states
        for j=1:steps
            mat(i,j) = states(k,i,j);
        end
    end
    x = 1:steps;    
end
y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-', 'MarkerSize',8);

% 1
vet_error = zeros(257, 1);
for sample=196:196
    [states, relations_new, vet_weight] = simulation_flashback_direct( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress1, extintion_rate_supress1, learn_rate_trauma1, learn_rate_trauma2, extintion_rate_trauma1,  steepness, threshold, empiricaldata, sample, start_trauma, end_trauma, trigger_on_period, trigger_off_period, 5);
    time = 1000+empiricaldata(sample,10)*10;
    vet_error(sample-1) = abs(states(k,21,time) - empiricaldata(sample,9));
end
for k=1:number_of_agents  % define which agents plot. ex.: k=2:3 print behavior of agents 2 and 3.
    mat = zeros(number_states, steps);
    for i=1:number_states
        for j=1:steps
            mat(i,j) = states(k,i,j);
        end
    end
    x = 1:steps;    
end
y = mat(21,:); plot(x(1:350:steps), y(1:350:steps), '--', 'MarkerSize',8);


% 2
vet_error = zeros(257, 1);
for sample=196:196
    [states, relations_new, vet_weight] = simulation_flashback_direct( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress1, extintion_rate_supress1, learn_rate_trauma1, learn_rate_trauma2, extintion_rate_trauma1,  steepness, threshold, empiricaldata, sample, start_trauma, end_trauma, trigger_on_period, trigger_off_period, 6);
    time = 1000+empiricaldata(sample,10)*10;
    vet_error(sample-1) = abs(states(k,21,time) - empiricaldata(sample,9));
end
for k=1:number_of_agents  % define which agents plot. ex.: k=2:3 print behavior of agents 2 and 3.
    mat = zeros(number_states, steps);
    for i=1:number_states
        for j=1:steps
            mat(i,j) = states(k,i,j);
        end
    end
    x = 1:steps;    
end
y = mat(21,:); plot(x(1:200:steps), y(1:200:steps), '-O', 'MarkerSize',8);


graph_legend = legend('No Trigger', 'Several Triggers', 'Constant Trigger');
set( graph_legend, 'FontSize', 22);
hold off;
