function [ret_value] = combination_function_alogistic(i,  k, state, speed_factor, delta_t, t, impactsum, steepness, threshold)
    aggimpact  = ( (1 / ((1+exp(-steepness*(impactsum(i)-threshold))))) - (1 / (1+exp(steepness*threshold))) ) * (1 + exp(-steepness*threshold));
    if aggimpact < 0
        aggimpact = 0;
    end
    ret_value = state(k,i,t-1) + speed_factor*(aggimpact-state(k,i,t-1))*delta_t;
    if ret_value<0 ret_value = 0; end
end