% Simulation.
function [ret_state, ret_relations, ret_weight]= flashback_register196_simulation(number_of_agents, number_states, relations, steps, delta_t, speed_factor, learn_rate_supress, extintion_rate_supress, learn_rate_trauma, extintion_rate_trauma, empiricaldata, sample)
    
    W_vet = zeros(steps,1);
    state = zeros(number_of_agents, number_states, steps);


    for t = 2:steps
       
        for k=1:number_of_agents % controls the simulation of each agent.
            % AGGIMPACT: total weight of inputs for each state.
            impact_tot = zeros(number_states);
            for i = 1:number_states
                for j = 1:number_states
                    impact_tot(i) = impact_tot(i) + relations(k,j,i);
                end
            end

            % update hebbian connections: Fear extinction learning to get rid of the trauma.
            Wc_f = relations(k,17,18) - (learn_rate_supress*state(k,17,t-1)*state(k,18,t-1)*(1-relations(k,17,18))- extintion_rate_supress*relations(k,17,18))*delta_t;
            Wf_c = relations(k,18,17) + (learn_rate_supress*state(k,18,t-1)*state(k,17,t-1)*(1-relations(k,18,17))- extintion_rate_supress*relations(k,18,17))*delta_t;
            Wc_p = relations(k,17,19) - (learn_rate_supress*state(k,17,t-1)*state(k,19,t-1)*(1-relations(k,17,19))- extintion_rate_supress*relations(k,17,19))*delta_t;
            Wp_c = relations(k,19,17) + (learn_rate_supress*state(k,19,t-1)*state(k,17,t-1)*(1-relations(k,19,17))- extintion_rate_supress*relations(k,19,17))*delta_t;
            relations(k,17,18) = Wc_f;
            relations(k,18,17) = Wf_c;
            relations(k,17,19) = Wc_p;
            relations(k,19,17) = Wp_c;

            % update hebbian connections: Learning acquiring the trauma
            Wsrt_p    = relations(k,9,19) + (learn_rate_trauma*state(k,9,t-1) *state(k,19,t-1)*(1-relations(k,9,19))- (extintion_rate_trauma*relations(k,9,19)))*delta_t;
            Wp_srt    = relations(k,19,9) + (learn_rate_trauma*state(k,19,t-1)*state(k,9,t-1)*(1-relations(k,19,9)) - (extintion_rate_trauma*relations(k,19,9)))*delta_t;
            Wsrtr_p   = relations(k,12,19) + (learn_rate_trauma*state(k,12,t-1) *state(k,19,t-1)*(1-relations(k,12,19))- (extintion_rate_trauma*relations(k,12,19)))*delta_t;
            Wsrt_srtr = relations(k,9,12)  + (learn_rate_trauma*state(k,9,t-1) *state(k,12,t-1) *(1-relations(k,9,12)) - (extintion_rate_trauma*relations(k,9,12)))*delta_t;
            Wsrtr_srt = relations(k,12,9)  + (learn_rate_trauma*state(k,12,t-1) *state(k,9,t-1)*(1-relations(k,12,9))  - (extintion_rate_trauma*relations(k,12,9)))*delta_t;
            relations(k,9,19) = Wsrt_p;
            relations(k,19,9) = Wp_srt;
            relations(k,12,19) = Wsrtr_p;
            relations(k,9,12)  = Wsrt_srtr;
            relations(k,12,9)  = Wsrtr_srt;

            % df db
            W_vet(t) = Wsrtr_p;


             % AGGIMPACT: sum of weights * value states.
             impactsum = zeros(number_states, 1);
             for i = 1:number_states
                 for j = 1:number_states
                     impactsum(j) = impactsum(j) + state(k, i, t-1) * relations(k,i,j);
                 end
             end
 
             % Calculation of aggimpact and stepwise simulation for all states.
             for i = 1:number_states
                 if impact_tot(i)>=-0.001 && impact_tot(i)<=0.001
                     aggimpact = 0;
                 else
                     if i==2 || i==5 || i==8 || i==11 || i==14 || i==3 || i==6 || i==15 || i==20 || i==21
                        aggimpact = impactsum(i); 
                     else
                        aggimpact = impactsum(i) / impact_tot(i);
                     end
                     if aggimpact<0 aggimpact = 0; end
                 end
                 
                 state(k,i,t) = state(k,i,t-1) + speed_factor*(aggimpact-state(k,i,t-1))*delta_t;
                 if state(k,i,t)<0 state(k,i,t) = 0; end
             end
             
             
            % REGURATE EXTERNAL SITUATION
            impactsum_execution_state = 0;
            for l=1:number_of_agents
                if l~=k % sum only the other agents and step over itself.
                    impactsum_execution_state = impactsum_execution_state + (state(l,19,t-1)/(number_of_agents - 1));
                end
            end
            
            % TUNE: Social Help group POSITIVE %impactsum_execution_state;
            if t>1
                state(k,1,t)= empiricaldata(sample,5);
                for n=1:number_of_agents
                    state(k,1,t) = state(k,1,t) + state(n,20,t-1);
                end
            else
                state(k,1,t) = 0.0; 
            end
            % TUNE: Social Help group NEGATIVE  %impactsum_execution_state;
            if t>1 state(k,4,t) = empiricaldata(sample,4); else state(k,4,t) = 0; end
            % TUNE: Trauma event. just once in all simulation time.
            if (t>500 && t<=1000) state(k,7,t)= empiricaldata(sample,6); else state(k,7,t) = 0.0; end
            % TUNE: trigger event.
            if t>1000 state(k,10,t) = empiricaldata(sample,7); else state(k,10,t) = 0; end
            % TUNE: environment, always influencing.
            if t>1 state(k,13,t) = empiricaldata(sample,8); else state(k,13,t) = 0; end
        end
    end
    ret_state  = state;
    ret_relations  = relations;
    ret_weight = W_vet;
end