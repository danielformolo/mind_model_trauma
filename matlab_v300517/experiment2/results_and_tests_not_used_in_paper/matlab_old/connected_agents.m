%----------------------------------------------
% INITIAL SETUP
clear
close all
clc
empiricaldata = importfile('samples.mat');

delta_t = 0.08;
speed_factor = 0.04;
number_of_agents = 4; % TUNE: number of agents
steps = 5000;         % TUNE: rounds, global end time
number_states = 21;

%
% TUNE: FLASHBACK AGENT
%
 learn_rate_supress1 = 0.2;      % hebbian connection
 extintion_rate_supress1 = 0.045; % hebbian connection
 learn_rate_trauma1 = 0.8;       % hebbian connection
 extintion_rate_trauma1 = 0.02;  % hebbian connection
%
% FOR SAMPLE 196, register #625, relation 12->19 started very low: 0.01 and finished high:
% 0.8085 characterizing a flashback personality with strong connection
% between preparation state and trigger events.
%samples: G1
%
relations1 = [0  , 0.5, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0.1, 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,-0.5,    0.1,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0.5, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 4
              0  , 0  , 0  , 0  ,   0  , 0.9, 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.9, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; % 8
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.01,   0  , 0  , 0  , 0.5,   0.01,  0  ,0.01, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.5, 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.9,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.01,0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.5,  0  ,0.01, 0  ,   0 ; %12
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0.5, 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0.2, 0  ,    0  ,  0  , 0  , 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.5,  0  ,-0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  ,0.1,     0  ,  0  , 0  , 0.5,   0  ; %16
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  , -0.001,0  , 0  ,-0.5,   0  , 0  , 0  ,-0.5,    0  ,-0.01,-0.01, 0  ,  0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0.5,    0.01, 0  , 0.5, 0  ,   0  ;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0.4, 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0.01, 0.99, 0  , 0  ,   0.9;
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ; %20
              0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,   0  , 0  , 0  , 0  ,    0  ,  0  , 0  , 0  ,   0  ;];

mrelations = zeros( number_of_agents, number_states, number_states);
for k=1:number_of_agents
    for i=1:number_states
        for j=1:number_states
            if (mod(k,2)==1)
                mrelations(k,i,j) = relations1(i,j);
            else
                mrelations(k,i,j) = relations1(i,j);
            end
        end
    end
end

%----------------------------------------------
% SIMULATION
%vet_samples = [55 75 196 199 234 2 16 71 73 86];
vet_samples = [199 55 75 196];
vet_out = zeros(10, steps);
for sample=1:4
    mrelations = zeros( number_of_agents, number_states, number_states);
    for k=1:number_of_agents
        for i=1:number_states
            for j=1:number_states
                if (sample < 6)
                    mrelations(k,i,j) = relations1(i,j);
                else
                    mrelations(k,i,j) = relations3(i,j);
                end
            end
        end
    end
    if (sample < 6)
        [states, relations_new]= connected_agents_simulation( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress1, extintion_rate_supress1, learn_rate_trauma1, extintion_rate_trauma1, empiricaldata, vet_samples(sample));
    else
        [states, relations_new]= connected_agents_simulation( number_of_agents, number_states, mrelations, steps, delta_t, speed_factor, learn_rate_supress3, extintion_rate_supress3, learn_rate_trauma3, extintion_rate_trauma3, empiricaldata, vet_samples(sample));
    end
    for i=1:steps
        vet_out(sample,i) = states(k,21,i);
    end

end

hold all;
xlabel('time') 
ylabel('states value')
for k=1:number_of_agents
    if k<6 && k>1
        plot(vet_out(k,:), 'k');
        plot(1000+empiricaldata(vet_samples(k),10)*10,empiricaldata(vet_samples(k),9),'k*');
    else
        plot(vet_out(k,:), 'b');
        plot(1000+empiricaldata(vet_samples(k),10)*10,empiricaldata(vet_samples(k),9),'b*');
    end
end
hold off;
