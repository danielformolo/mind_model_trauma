% Simulation.
function [ret_state, ret_relations]= connected_agents_simulation(number_of_agents, number_states, relations, steps, delta_t, speed_factor, learn_rate_supress, extintion_rate_supress, learn_rate_trauma_w18_w28, learn_rate_trauma_w17_w27, extintion_rate_trauma, steepness, threshold, empiricaldata, sample, trauma_start, trauma_end, trigger_active_period, trigger_end_period, t2_env)
    
    W_vet = zeros(9,steps);
    state = zeros(number_of_agents, number_states, steps);

    frequency_size = 0;
    frequency_size_activate = 0;

    for t = 2:steps
        for k=1:number_of_agents % controls the simulation of each agent.
            % update hebbian connections: Fear extinction learning to get rid of the trauma.
            Wc_f = relations(k,17,18) - (learn_rate_supress*state(k,17,t-1)*state(k,18,t-1)*(1-relations(k,17,18))- extintion_rate_supress*relations(k,17,18))*delta_t;
            Wf_c = relations(k,18,17) + (learn_rate_supress*state(k,18,t-1)*state(k,17,t-1)*(1-relations(k,18,17))- extintion_rate_supress*relations(k,18,17))*delta_t;
            Wc_p = relations(k,17,19) - (learn_rate_supress*state(k,17,t-1)*state(k,19,t-1)*(1-relations(k,17,19))- extintion_rate_supress*relations(k,17,19))*delta_t;
            Wp_c = relations(k,19,17) + (learn_rate_supress*state(k,19,t-1)*state(k,17,t-1)*(1-relations(k,19,17))- extintion_rate_supress*relations(k,19,17))*delta_t;
            relations(k,17,18) = Wc_f;
            relations(k,18,17) = Wf_c;
            relations(k,17,19) = Wc_p;
            relations(k,19,17) = Wp_c;

            % update hebbian connections: Learning acquiring the trauma
            Wsrt_p    = relations(k,9,19)  + (learn_rate_trauma_w18_w28*state(k,9,t-1) *state(k,19,t-1)*(1-relations(k,9,19))- (extintion_rate_trauma*relations(k,9,19)))*delta_t;
            Wp_srt    = relations(k,19,9)  + (learn_rate_trauma_w17_w27*state(k,19,t-1)*state(k,9,t-1)*(1-relations(k,19,9)) - (extintion_rate_trauma*relations(k,19,9)))*delta_t;
            Wsrtr_p   = relations(k,12,19) + (learn_rate_trauma_w17_w27*state(k,12,t-1) *state(k,19,t-1)*(1-relations(k,12,19))- (extintion_rate_trauma*relations(k,12,19)))*delta_t;
            Wsrt_srtr = relations(k,9,12)  + (learn_rate_trauma_w18_w28*state(k,9,t-1) *state(k,12,t-1) *(1-relations(k,9,12)) - (extintion_rate_trauma*relations(k,9,12)))*delta_t;
            Wsrtr_srt = relations(k,12,9)  + (learn_rate_trauma_w18_w28*state(k,12,t-1) *state(k,9,t-1)*(1-relations(k,12,9))  - (extintion_rate_trauma*relations(k,12,9)))*delta_t;
            relations(k,9,19) =  Wsrt_p;
            relations(k,19,9) =  Wp_srt;
            relations(k,12,19) = Wsrtr_p;
            relations(k,9,12)  = Wsrt_srtr;
            relations(k,12,9)  = Wsrtr_srt;
            
            W_vet(1,t) = Wc_f;
            W_vet(2,t) = Wf_c;
            W_vet(3,t) = Wc_p;
            W_vet(4,t) = Wp_c;
            W_vet(5,t) = Wsrt_p;
            W_vet(6,t) = Wp_srt;
            W_vet(7,t) = Wsrtr_p;
            W_vet(8,t) = Wsrt_srtr;
            W_vet(9,t) = Wsrtr_srt;

            % AGGIMPACT: sum of weights * value states.
            impactsum = zeros(number_states, 1);
            for i = 1:number_states
                for j = 1:number_states
                    impactsum(j) = impactsum(j) + state(k, i, t-1) * relations(k,i,j);
                end
            end
            
          % Calculation of aggimpact and stepwise simulation for all states.
          % state(k,01,t) SOCIAL SUPPORT input below defined.
            state(k,02,t) = combination_function_id(02, k, state, speed_factor, delta_t, t, impactsum);
            state(k,03,t) = combination_function_id(03, k, state, speed_factor, delta_t, t, impactsum);
          % state(k,04,t) NEGATIVE CONTAGION input below defined.
            state(k,05,t) = combination_function_id(05, k, state, speed_factor, delta_t, t, impactsum);
            state(k,06,t) = combination_function_id(06, k, state, speed_factor, delta_t, t, impactsum);
          % state(k,07,t) TRAUMA input below defined.
            state(k,08,t) = combination_function_id(08, k, state, speed_factor, delta_t, t, impactsum);
            state(k,09,t) = combination_function_alogistic(09, k, state, speed_factor, delta_t, t, impactsum, steepness, threshold);
          % state(k,10,t) TRIGGER input below defined.
            state(k,11,t) = combination_function_id(11, k, state, speed_factor, delta_t, t, impactsum);
            state(k,12,t) = combination_function_alogistic(12, k, state, speed_factor, delta_t, t, impactsum, steepness, threshold);            
          % state(k,13,t) ENVIRONMENT input below defined.
            state(k,14,t) = combination_function_id(14, k, state, speed_factor, delta_t, t, impactsum);
            state(k,15,t) = combination_function_id(15, k, state, speed_factor, delta_t, t, impactsum);
            state(k,16,t) = combination_function_alogistic(16, k, state, speed_factor, delta_t, t, impactsum, steepness, threshold);
            state(k,17,t) = combination_function_alogistic(17, k, state, speed_factor, delta_t, t, impactsum, steepness, threshold);
            state(k,18,t) = combination_function_alogistic(18, k, state, speed_factor, delta_t, t, impactsum, steepness, threshold);
            state(k,19,t) = combination_function_alogistic(19, k, state, speed_factor, delta_t, t, impactsum, steepness, threshold);
            state(k,20,t) = combination_function_alogistic(20, k, state, speed_factor, delta_t, t, impactsum, steepness*2, threshold/10);
            state(k,21,t) = combination_function_id(21, k, state, speed_factor, delta_t, t, impactsum);

           % INPUTS
            impactsum_execution_state = 0;
            for l=1:number_of_agents
                if l~=k % sum only the other agents and step over itself.
                    impactsum_execution_state = impactsum_execution_state + (state(l,19,t-1)/(number_of_agents - 1));
                end
            end
            
            
                % TUNE: Social Help group POSITIVE %impactsum_execution_state;
                if t>1 && number_of_agents>1  && t2_env ~=3
                    med = 0;
                    for n=1:number_of_agents
                        med = med + state(n,20,t-1);
                    end
                    state(k,1,t) = med / number_of_agents;
                else
                    %state(k,1,t)= empiricaldata(sample,5);
                    state(k,1,t) = 0;
                end
                
                % TUNE: Social Help group NEGATIVE  %impactsum_execution_state;
                if t>1 && number_of_agents>1 && t2_env ~=3
                    med = 0;
                    for n=1:number_of_agents
                        med = med + state(n,20,t-1);
                    end
                    state(k,4,t) = med / number_of_agents;
                else
                    %state(k,4,t) = empiricaldata(sample,5);
                    state(k,4,t) = 0;
                end


%             % TUNE: Social Help group POSITIVE %impactsum_execution_state;
%             if t>1
%                 state(k,1,t)= empiricaldata(sample,5);
%                 if number_of_agents>1
%                     for n=1:number_of_agents
%                         state(k,1,t) = state(k,1,t) + state(n,20,t-1);
%                     end
%                 end
%             else
%                 state(k,1,t) = 0.0; 
%             end
%             
%             % TUNE: Social Help group NEGATIVE  %impactsum_execution_state;
%             if t>1 state(k,4,t) = empiricaldata(sample,4); else state(k,4,t) = 0; end
%             % TUNE: Trauma event. just once in all simulation time.



            %if (t>trauma_start && t<=trauma_end) state(k,7,t)= empiricaldata(sample,6); else state(k,7,t) = 0.0; end
            % TUNE: Trauma event. 
                if t2_env==0 || t2_env==3 
                    if (t>500 && t<=1000) state(k,7,t)= empiricaldata(sample,6); else state(k,7,t) = 0.0; end
                elseif t2_env==1 
                    if (t>500 && t<=1000) state(k,7,t)= empiricaldata(sample,6); else state(k,7,t) = 0.0; end
                    if k==3
                        state(k,7,t)= empiricaldata(sample,6);
                    end
                end
            
            
            
            
            
            
            
            
            
            % TUNE: trigger event.
            if (t>trauma_end && frequency_size>0)
                state(k,10,t) = empiricaldata(sample,7); 
                frequency_size = frequency_size - 1;
            else
                if t>trauma_end
                    frequency_size_activate = frequency_size_activate + 1;
                    if frequency_size_activate>trigger_end_period
                        frequency_size = trigger_active_period;
                        frequency_size_activate = 0;
                    end
                elseif (t>trauma_start && t<=trauma_end)
                    state(k,10,t) = 1;
                else
                    state(k,10,t) = 0;
                end
            end
            % TUNE: environment, always influencing.
            if t>1 state(k,13,t) = empiricaldata(sample,8); else state(k,13,t) = 0; end
        end
    end
    ret_state  = state;
    ret_relations  = relations;
    ret_weight = W_vet;
end